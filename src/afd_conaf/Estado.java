/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package afd_conaf;

import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class Estado {
    
    String description;
    ArrayList<Evento> eventos;
    
    public Estado(String description){
        this.description = description;
        eventos = new ArrayList();
    }
    
    public void addEvento(Evento evento){
        this.eventos.add(evento);
    }
    
    public String getDescripcion(){
        return this.description;
    }

    public ArrayList<Evento> getEventos(){
        return this.eventos;
    }
    
    public String[] getOptionsText(){
        String[] salida = new String[eventos.size()];
        for(int i = 0;  i<eventos.size();   i++)
            salida[i] = "["+i+"] "+eventos.get(i).getDescripcion();
        return salida;
    }
    
}
