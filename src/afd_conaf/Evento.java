/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package afd_conaf;

/**
 *
 * @author jorge
 */
public class Evento {
    
    String description;
    Estado estado;//Al que lleva si se ejecuta este evento
    
    public Evento(String description, Estado estado){
        this.description = description;
        this.estado = estado;
    }
    
    public String getDescripcion(){
        return this.description;
    }
    
    public Estado ejecutar(){
        return this.estado;
    }
    
}
