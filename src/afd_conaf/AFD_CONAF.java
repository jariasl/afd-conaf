/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package afd_conaf;

import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class AFD_CONAF {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        AFD determinista = new AFD();
        
        Scanner keyboard = new Scanner(System.in);//InputStream
        
        System.out.println("Maquina inicializada");
        System.out.println("====================\n");
        
        System.out.println("Estado inicial: "+determinista.getS().getDescripcion());
        
        while(!determinista.isTerminado()){
            
            System.out.println("Ingrese operación:");
            for(String text : determinista.getEstadoActual().getOptionsText()){
                System.out.println("\t"+text);
            }
            
            int entrada = -1;
            do{
                try {
                    //Pedir entrada evento
                    entrada = keyboard.nextInt();
                    
                    try{
                        //Ejecutar evento y avanzar de estado
                        determinista.ejecutarFuncionTransicion(entrada);
                    }catch (Exception e){
                        System.out.println("Error: "+e.getMessage());
                        entrada = -1;
                    }
                } catch (Exception e) {
                    System.out.println("Valor erróneo. Intentar nuevamente...");
                    keyboard.next();//Evitar quedar en loop de exception
                }
            }while(!(entrada >= 0));
            
            System.out.println("\nEstado actual: q"+determinista.getIndexEstadoActual()+" - "+determinista.getEstadoActual().getDescripcion());
            
        }
        
        if(determinista.isTerminado()){
            System.out.println("Analisis Términado!\n"
                    + "... en "+determinista.getTransiciones()+" transiciones.");
        }
        
    }
    
}
