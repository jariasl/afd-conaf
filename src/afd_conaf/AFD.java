/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package afd_conaf;

import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class AFD {
    ArrayList<Estado> Q;
    ArrayList<Evento> V;
    Estado s;//Estado inicial
    Estado f;//Estado final
    Estado actual;
    
    int cantOperaciones;
    

    public AFD() {
        
        //Estados puros
        Estado q0 = new Estado("Recibida notificación de incendio");
        Estado q1 = new Estado("Información analizada");
        Estado q2 = new Estado("Estrategia determinada");
        Estado q3 = new Estado("Plan establecido");
        Estado q4 = new Estado("Coordinados");
        Estado q5 = new Estado("Abastecidos");
        Estado q6 = new Estado("Entidades vinculadas");
        Estado q7 = new Estado("Incendio controlado");
        Estado q8 = new Estado("Incendio extinguido");
        
        Q = new ArrayList();
        Q.add(q0);
        Q.add(q1);
        Q.add(q2);
        Q.add(q3);
        Q.add(q4);
        Q.add(q5);
        Q.add(q6);
        Q.add(q7);
        Q.add(q8);
        
        //Eventos y estados a los que llevan
        Evento e0 = new Evento("Analizar",q1);
        Evento e1 = new Evento("Determinar estrategia",q2);
        Evento e2 = new Evento("Establecer plan de supresión",q3);
        Evento e3 = new Evento("Coordinar recursos",q4);
        Evento e4 = new Evento("Proveer abastecimiento",q5);
        Evento e5 = new Evento("Coordinar entidades",q6);
        Evento e6 = new Evento("Controlar incendio",q7);
        Evento e7 = new Evento("Estinguir incendio",q8);
        
        V = new ArrayList();
        V.add(e0);
        V.add(e1);
        V.add(e2);
        V.add(e3);
        V.add(e4);
        V.add(e5);
        V.add(e6);
        V.add(e7);
        
        //Asignar eventos que se pueden ejecutar a sus estados
        q0.addEvento(e0);
        q1.addEvento(e1);
        q2.addEvento(e2);
        q3.addEvento(e3);
        q4.addEvento(e4);
        q5.addEvento(e5);
        q6.addEvento(e6);
        q7.addEvento(e6);
        q7.addEvento(e7);
        
        //COMO EJEMPLO AGREGAMOS QUE LLUVIA CONTROLA EL FUEGO
        q0.addEvento(new Evento("Lluvia controla incendio", q7));
        q7.addEvento(new Evento("Se expande", q0));
        
        //Configurar estado inicial y final
        s = q0;
        f = q8;
        actual = s;
        cantOperaciones = 0;
        
    }
    
    public void ejecutarFuncionTransicion(int entrada) {
        if(!isTerminado()){
            if(0 <= entrada && entrada < actual.eventos.size()){
                actual = actual.eventos.get(entrada).ejecutar();
                cantOperaciones += 1;
            }else{
                throw new IllegalStateException("Evento no existe");
            }
        }
    }

    public Estado getS() {
        return s;
    }

    public Estado getF() {
        return f;
    }
    
    public Estado getEstadoActual(){
        return actual;
    }
    
    public int getIndexEstadoActual(){
        return Q.indexOf(actual);
    }

    public int getTransiciones() {
        return cantOperaciones;
    }
    
    public boolean isTerminado(){
        return actual==f;
    }
    
}
